package net.hitpartner.app

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.login_activity.*
import net.hitpartner.app.di.LoginPresenter
import javax.inject.Inject

/**
 * Created by Rakesh Das on 1/20/18.
 */
class LoginActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var app: HitPartnerApplication

    @Inject
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())

        login_btn.setOnClickListener {
            presenter.submitLoginDetails(this)
        }
    }

    private fun getLayoutResId():Int = R.layout.login_activity


}