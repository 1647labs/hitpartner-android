package net.hitpartner.app.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.hitpartner.app.HitPartnerActivity
import net.hitpartner.app.LoginActivity

/**
 * Created by Rakesh Das on 1/13/18.
 */
@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = arrayOf(HitPartnerActivityModule::class))
    abstract fun inject_HitPartnerActivity(): HitPartnerActivity

    @ContributesAndroidInjector(modules = arrayOf(LoginActivityModule::class))
    abstract fun inject_LoginActivity(): LoginActivity
}