package net.hitpartner.app.di

import android.content.Context

/**
 * Created by Rakesh Das on 1/20/18.
 */
interface LoginActivityContract {

    interface LoginActivityView {
        // button events
        fun onLoginButtonClicked()
    }
    interface LoginActivityPresenter {
        fun submitLoginDetails(context: Context)
    }
}