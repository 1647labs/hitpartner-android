package net.hitpartner.app.di

import dagger.Binds
import dagger.Module

/**
 * Created by Rakesh Das on 1/13/18.
 */
@Module
abstract class LoginActivityModule {
    @Binds
    abstract fun loginPresenter(presenter: LoginPresenter): LoginActivityContract.LoginActivityPresenter
}