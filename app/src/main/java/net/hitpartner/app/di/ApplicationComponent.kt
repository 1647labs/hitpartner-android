package net.hitpartner.app.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import net.hitpartner.app.HitPartnerApplication
import javax.inject.Singleton

/**
 * Created by Rakesh Das on 1/13/18.
 */
@Singleton
@Component(modules = arrayOf(
        ApplicationModule::class,
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class
))
interface ApplicationComponent : AndroidInjector<HitPartnerApplication> {
    override fun inject(instance: HitPartnerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: HitPartnerApplication):ApplicationComponent.Builder
        fun build(): ApplicationComponent
    }



}