package net.hitpartner.app

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import net.hitpartner.app.di.DaggerApplicationComponent

/**
 * Created by markusmcgee on 1/13/18.
 */
class HitPartnerApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val applicationComponent = DaggerApplicationComponent.builder().application(this).build()
        applicationComponent.inject(this)
        return applicationComponent
    }

}