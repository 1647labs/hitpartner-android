package net.hitpartner.app

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Created by Rakesh Das on 1/13/18.
 */
class HitPartnerActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var app: HitPartnerApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
    }
    override fun onResume() {
        super.onResume()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun getLayoutResId():Int = R.layout.hitpartner_activity


}